#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "multiregex.h"

void
help()
{
	printf(
"grep-like program that accepts logical expression (and/or/not) with regular\n"
"expressions\n"
"\n"
"usage: multigrep [-h] [-0] <pattern args>\n"
"\n"
"-h to print this help, -0 to separate input by null bytes (\\0) rather than\n"
"by newlines (\\n)\n"
"<pattern args> are passed as separate arguments, and are on of\n"
"\n"
"    [, and[, AND[\n"
"       Start of logical AND group - there is no difference between [,\n"
"       and[, AND[ but you can use the latter ones to be consistent with\n"
"       or[, OR[\n"
"\n"
"    or[, OR[\n"
"        Start of logical OR group\n"
"\n"
"    ]\n"
"        end of logical OR/AND group\n"
"\n"
"    !\n"
"        logical NOT - applies to the following AND group / OR group / regexp\n"
"\n"
"    Anything else is treated as a regular expression. The \"top level\" is\n"
"    treated as an AND expression.\n"
"\n"
"Example:\n"
"\n"
"  multigrep ^Foo: OR[ WARNING ERROR AND[ INFO FooInfoMessage ] ] ! FooBarException\n"
"\n"
"Will pass through all lines that start with Foo:, contain WARNING or ERROR\n"
"or both INFO and FooInfoMessage, and do not contain FooBarException.\n"
	);
}

int
main(int argc, char **argv)
{
	char *buf = NULL; /* stdin buffer */
	size_t nbuf = 0;  /* stdin buffer length */
	multiregex *filter; /* multiregex used to filter stdin */
	int opt_zero = 0; /* if true, separates input lines by '\0', not '\n' */
	int args_start = 1; /* start of multiregex filter definition */

	if (argc >= 2 && 0 == strcmp("-h", argv[1])) {
		help();
		return 0;
	}
	if (argc >= 2 && 0 == strcmp("-0", argv[1])) {
		++args_start;
		opt_zero = 1;
	}
	if (argc - args_start < 1) {
		help();
		return 1;
	}

	filter = multiregex_compile(argv+args_start, argc-args_start, 0);
	if (opt_zero) while ( -1 != getdelim(&buf, &nbuf, '\0', stdin)) {
		if (multiregex_eval(filter, buf)) {
			fputs(buf, stdout);
			putc('\0', stdout);
		}
	}
	else while (-1 != getline(&buf, &nbuf, stdin)) {
		if (multiregex_eval(filter, buf)) fputs(buf, stdout);
	}
	multiregex_free(filter);
	return 0;
}
