OBJS=main.o multiregex.o
CC=gcc
CFLAGS= -march=native -std=c11 -Wall -Wpedantic -D_POSIX_C_SOURCE=200809L
LDFLAGS=
LIBS=

all: CFLAGS ::= $(CFLAGS) -O3
all: multigrep

debug: CFLAGS ::= $(CFLAGS) -Og -g -DDEBUG
debug: multigrep

multigrep: $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

clean:
	rm -f $(OBJS)
