#ifndef MULTIREGEX_H
#define MULTIREGEX_H

#include <regex.h>
#include <sys/types.h>

#define EXPR_OR   0x01
#define EXPR_AND  0x02
#define EXPR_REG  0x03
#define EXPR_END  0x04

#define EXPR_TYPE_MASK 0x0F
#define EXPR_NEGATION_MASK 0x10

struct expr_name_pair {
	char *name;
	int type;
};

extern const struct expr_name_pair EXPR_NAMES[];
#define EXPR_NAMES_ARR_DEF { \
 	{"or[",  EXPR_OR },  \
 	{"OR[",  EXPR_OR },  \
 	{"and[", EXPR_AND},  \
 	{"AND[", EXPR_AND},  \
 	{"[",    EXPR_AND},  \
 	{"]",    EXPR_END},  \
	{ NULL, 0 } \
 }

typedef struct multiregex {
	int type;
	int negated;
	union {
		regex_t *reg;
		struct multiregex *internal;
	};
	struct multiregex *next;
	/* struct multiregex *prev; */
} multiregex;

multiregex*
multiregex_compile(char **args, int nargs, int cflags);

int
multiregex_eval(const multiregex *mreg, const char *str);

void
multiregex_free(multiregex *mreg);

#endif
