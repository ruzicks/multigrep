#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "multiregex.h"

const struct expr_name_pair EXPR_NAMES[] = EXPR_NAMES_ARR_DEF;

multiregex*
multiregex_compile(char **args, int nargs, int cflags)
{
	int arg_i; /* position in args */
	multiregex **mreg_stack; /* stack of multiregex elements */
	int mreg_stack_top = 1; /* top element in the stack */
	cflags |= REG_NOSUB; /* do not return matched substring(s) */

	mreg_stack = calloc(sizeof(multiregex*), nargs);
	mreg_stack[0] = malloc(sizeof(multiregex));
	mreg_stack[0]->type = EXPR_AND;
	/* mreg_stack[0]->prev = NULL; */
	mreg_stack[0]->next = NULL;

	int next_negated = 0;
	for (arg_i = 0; arg_i < nargs; ++arg_i) {
		char *arg = args[arg_i]; /* current argument */
		multiregex current = {0}; /* current element (on stack) */
		int i;
		if (next_negated) current.negated = 1;
		next_negated = 0;

		/* current.prev = mreg_stack[mreg_stack_top]; */

		if (arg[0] == '!' && arg[1] == '\0') {
			next_negated = 1;
			continue;
		}
		for (i = 0; EXPR_NAMES[i].name != NULL; ++i) {
			if (0 == strcmp(EXPR_NAMES[i].name, arg)) {
				current.type = EXPR_NAMES[i].type;
				break;
			}
		}
		if (EXPR_END == current.type) {
			mreg_stack[mreg_stack_top--] = NULL;
			continue;
		}
		else if (EXPR_OR  == current.type ||
			 EXPR_AND == current.type) {
			multiregex *curr = malloc(sizeof(multiregex));
			memcpy(curr, &current, sizeof(multiregex));

			if (mreg_stack[mreg_stack_top]) {
				mreg_stack[mreg_stack_top]->next = curr;
			} else {
				mreg_stack[mreg_stack_top-1]->internal = curr;
			}

			mreg_stack[mreg_stack_top++] = curr;
			continue;
		}
		else {
			multiregex *curr; /* current element (on heap) */
			int res; /* regcomp result */

			current.type = EXPR_REG;
			current.reg = calloc(sizeof(regex_t), 1);
			if (0 != (res = regcomp(current.reg, arg, cflags))) {
				char err[255];
				regerror(res, current.reg, err, 255);
				fprintf(
				    stderr,
				    "Error while processing regex: `%s': %s\n",
				    arg,
				    err);
				goto multiregex_compile_error;
			}
			curr = malloc(sizeof(multiregex));
			memcpy(curr, &current, sizeof(multiregex));

			if (mreg_stack[mreg_stack_top]) {
				mreg_stack[mreg_stack_top]->next = curr;
			} else {
				mreg_stack[mreg_stack_top-1]->internal = curr;
			}

			mreg_stack[mreg_stack_top] = curr;
		}
	}
	return mreg_stack[0];

multiregex_compile_error:
	return NULL;
}

int
multiregex_eval(const multiregex *mreg, const char *str)
{
	int ret = 0;
	int is_negated = mreg->negated;
	switch (mreg->type) {
	case EXPR_OR:
		for (mreg = mreg->internal; mreg != NULL; mreg = mreg->next) {
			if (multiregex_eval(mreg, str)) {
				ret = 1;
				goto multiregex_eval_return;
			}
		}
		ret = 0;
		break;
	case EXPR_AND:
		for (mreg = mreg->internal; mreg != NULL; mreg = mreg->next) {
			if (!multiregex_eval(mreg, str)) {
				ret = 0;
				goto multiregex_eval_return;
			}
		}
		ret = 1;
		break;
	case EXPR_REG:
		ret = REG_NOMATCH != regexec(mreg->reg, str, 0, NULL, 0);
		break;
	default:
		assert(0);
	}

multiregex_eval_return:
	if (is_negated) return !ret;
	else return ret;
}

void
multiregex_free(multiregex *mreg)
{
	while (mreg != NULL) {
		switch (mreg->type) {
		case EXPR_AND:
		case EXPR_OR:
			if (mreg->internal) multiregex_free(mreg->internal);
			break;
		case EXPR_REG:
			regfree(mreg->reg);
			break;
		default:
			assert(0);
		}
		multiregex *next = mreg->next;
		free(mreg);
		mreg = next;
	}
}
