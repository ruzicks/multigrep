multigrep
=========

grep-like program that accepts logical expression (and/or/not) with regular
expressions

```
usage: multigrep [-h] [-0] <pattern args>

-h to print this help, -0 to separate input by null bytes (\0) rather than
by newlines (\n)
<pattern args> are passed as separate arguments, and are on of

    [, and[, AND[
       Start of logical AND group - there is no difference between [,
       and[, AND[ but you can use the latter ones to be consistent with
       or[, OR[

    or[, OR[
        Start of logical OR group

    ]
        end of logical OR/AND group

    !
        logical NOT - applies to the following AND group / OR group / regexp

    Anything else is treated as a regular expression. The "top level" is
    treated as an AND expression.

Example:

  multigrep ^Foo: OR[ WARNING ERROR AND[ INFO FooInfoMessage ] ] ! FooBarException

Will pass through all lines that start with Foo:, contain WARNING or ERROR
or both INFO and FooInfoMessage, and do not contain FooBarException.
```
